import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
;

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.page.html',
  styleUrls: ['./resultado.page.scss'],
})
export class ResultadoPage implements OnInit {

  dificultat: string;
  result: string;
  title: string;
  subtitle: string;

  constructor(private route: ActivatedRoute, private router: Router) { 

    this.route.queryParams.subscribe(params => {
      if (params && params.dificultat){
          this.dificultat = params.dificultat;
      }
      if (params && params.title){
        this.title= params.title;
      }
      if (params && params.subtitle){
        this.subtitle= params.subtitle;
      }
    });

  }
  resultado(){

    if(this.result == 'w'){
        document.getElementById("title").innerHTML = this.title;
        document.getElementById("subtitle").innerHTML = this.subtitle;

    }else{
      document.getElementById("title").innerHTML = this.title;
      document.getElementById("subtitle").innerHTML = this.subtitle;

    }

  }


  juego(){
    console.log("play")
    let navigationExtras: NavigationExtras = {
      queryParams: {
        dificultat: this.dificultat

      }

    };
    this.router.navigate(['juego'], navigationExtras)    
    .then(() => {
      window.location.reload();
    });
  }

  menu(){
    console.log("menu")
    let navigationExtras: NavigationExtras = {

    };
    this.router.navigate(['menu-juego'], navigationExtras)
    .then(() => {
      window.location.reload();
    });
  }

  ngOnInit() {
  }

}
