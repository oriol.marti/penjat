import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { PalabraService } from 'src/app/services/palabra.service';

@Component({
  selector: 'app-juego',
  templateUrl: './juego.page.html',
  styleUrls: ['./juego.page.scss'],
})
export class JuegoPage {

  dificultat: string;
  paraula: any; //palabra data service
  vidas: any; //vidas = 8;
  underscore = [];
  titlew = "!!FELICITATS!!";
  titlel = "!!LOSEEEER!!";
  wsub = 'Ho has conseguit';
  lsub = 'Torna-ho a intentar'; 

  constructor(private route: ActivatedRoute, private router: Router, public dataService: PalabraService) { 
    
    this.route.queryParams.subscribe(params => {
      if (params && params.dificultat){

          this.dificultat = params.dificultat;

      }

    });
    
    
  }
  diffchecker(){
    if(this.dificultat == "normal"){
      console.log("normal")
      this.vidas = 8;
    }else if(this.dificultat == "dificil"){
      console.log("dificil")
      this.vidas = 5;
    }else {
      console.log("facil")
      this.vidas = 10;
    }
  }
  //funciona correctamente
  ocultador() {
    //underscore =Array.from(paraula)
    for (var i = 0; i < this.paraula.length; i++){
      if(this.underscore.length < this.paraula.length){
        this.underscore.push("_");
      }else {
        
      }
    }
    return this.underscore.join(" ");
    //devuelve la string asi --> palabra escondida ____
  }
  
  searchletter(letter, event) {
    event.srcElement.setAttribute("disabled",true);
    this.intento(letter);
    
  } 


  //funciona correctamente
  intento(letter){
    //var underarray = Array.from(this.underscore);
    var contador = 0;
    
    for(var i = 0; i < this.paraula.length; i++){
      if(letter == this.paraula[i]){
        
        this.underscore[i] = letter;

        contador++
      }else{
        //alert("letra incorrecta intenta de nuevo");
      }
    }
    if(this.paraula == this.underscore.join("")){

      console.log("end game")
    let navigationExtras: NavigationExtras = {

      queryParams: {
        dificultat: this.dificultat,
        title: this.titlew,
        subtitle: this.wsub
      }

    };
    //funciona correctamente
    this.router.navigate(['resultado'], navigationExtras);
    }else{
    if(this.vidas > 0){
    if(contador == 0){
      this.vidas--;
      
    }else {
      
    }}else {


      console.log("end game")
    let navigationExtras: NavigationExtras = {

      queryParams: {
        dificultat: this.dificultat,
        title: this.titlel,
        subtitle: this.lsub
      }

    };
    //funciona correctamente
    this.router.navigate(['resultado'], navigationExtras)

    }};
    return this.underscore;
  }
  ngOnInit() {

    this.paraula = this.dataService.getparaula();
    this.ocultador();
    this.diffchecker();
    
  }

};

