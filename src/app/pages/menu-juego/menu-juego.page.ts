import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-menu-juego',
  templateUrl: './menu-juego.page.html',
  styleUrls: ['./menu-juego.page.scss'],
})
export class MenuJuegoPage implements OnInit {
  dificultat: string= '';

  constructor(private router: Router,public alertController: AlertController) { }

  juego(){
    console.log("play")
    let navigationExtras: NavigationExtras = {

      queryParams: {
        dificultat: this.dificultat
      }

    };
    this.router.navigate(['juego'], navigationExtras);
  }
  async alert(){
    console.log("alert")
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Ajuda',
      message: '<p>Selecciona la dificultat del joc amb el desplegable situat sobre el boto de Jugar.</p> <p>Per juga apreta el boto "Jugar situat a sobre de aquest boto.</p>',
      
      buttons: ['Tanca']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role)

  }

  ngOnInit() {
  }

}
