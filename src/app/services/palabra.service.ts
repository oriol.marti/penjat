import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PalabraService {

  Arraypalabras = ['hola','barcelona','avio','avioneta'];
  numero: any;
  palabra: any;

  constructor() { 

  }

  getparaula(){
    this.numero = Math.floor(Math.random()*this.Arraypalabras.length);
    this.palabra = this.Arraypalabras[this.numero]

    return this.palabra;

  }

}
